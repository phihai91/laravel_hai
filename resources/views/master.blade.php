<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">

</head>
<body>
	<ul class="w3-navbar w3-black">
	  <li><a href="home">Home</a></li>
	  <li><a href="contact">Contact</a></li>
	</ul>

	@yield('content')

</body>
</html>